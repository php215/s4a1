<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity</title>
</head>
<body>
    <h1>Activity</h1>
    <h2>Building</h2>
    <p>The name of the building is <?php echo $building->getName(); ?>.</p>
    <p>The <?php echo $building->getName(); ?> has <?php echo $condo->getFloors(); ?> floors.</p>
    <p>The <?php echo $building->getName(); ?> is located at <?php echo $building->getAddress(); ?>.</p>
    <p>The name of the building has been changed to <?php echo $building->setName("Caswynn Complex")->getName(); ?>.</p>

    <h2>Condominium</h2>
    <p>The name of the condominium is <?php echo $condo->getName(); ?>.</p>
    <p>The <?php echo $condo->getName(); ?> has <?php echo $condo->getFloors(); ?> floors.</p>
    <p>The <?php echo $condo->getName(); ?> is located at <?php echo $condo->getAddress(); ?>.</p>
    <p>The name of the condominium has been changed to <?php echo $condo->setName("Enzo Tower")->getName(); ?>.</p>
</body>
</html>