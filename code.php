<?php

class Building{
    // private access modifier disables direct access to an  object's property or methods
    // protected access modifier allows inheritance of properties and methods to child classes. However, it will still disable direct access to its properties and methods.
    // private $name;
    // private $floors;
    // private $address;
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
    public function printName(){
        return "The name of the building is $this->name";
    }
    public function getName(){
        return $this->name;
    }
    
    public function setName($name){
        $this->name = $name;
        return $this;
    }
    
    public function getFloors(){
        return $this->floors;
    }
    
    public function setFloors($floors){
        $this->floors = $floors;
        return $this;
    }
    
    public function getAddress(){
        return $this->address;
    }

    public function setAddress($address){
        $this->address = $address;
        return $this;
    }
};

class Condo extends Building{

}

$building = new Building('Caswynn Building', 8,'Timog Avenue, Quezon City, Philippines');
$condo = new Condo('Enzo Condo', 5,'Buendia Avenue, Makati City, Philippines');